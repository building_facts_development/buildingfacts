import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from "./components/projectsPage.js";
import Aerial from "./components/aerial.js";
import BfMap from "./components/bfMap.js";
import CommercialVR from "./components/commercialVR.js";
import Contractors from "./components/contractors.js";
import Docs from "./components/docs.js";
import Folders from "./components/folders.js";
import Pictures from "./components/pictures.js";
import ProjectsPage from "./components/projectsPage.js";
import InfoCardsPage from "./components/infoCardsPage.js";
import Login from "./components/login.js";
import CreateAccount from "./components/createAcct.js";
import AddProject from "./components/addProject.js";
import ProjectInfo from "./components/projectInfo.js";
import { green, indigo, white } from '@material-ui/core/colors'
import { MuiThemeProvider, createMuiTheme, getContrastText } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: green['A700']
    },
    primary: {
      main: green[600],
    },
  },
  typography: {
    useNextVariants: true,
    fontFamily: [
      'Helvetica', 'sans-serif'
    ].join(',')
  },
  MuiTableCell: {
    fontSize: '1.2rem'
  }
});

class App extends Component {
  render() {
    return (
      <div>
        <MuiThemeProvider theme={theme}>
        	<Router>
      	    	<div>
      	    		<Route exact path = "/" component = {Home}/>
      	    		<Route path = "/aerial" component = {Aerial}/>
          		<Route path = "/bfMap" component = {BfMap}/>
          		<Route path = "/commercialVR" component = {CommercialVR}/>
          		<Route path = "/docs" component = {Docs}/>
          		<Route path = "/folders" component = {Folders}/>
          		<Route path = "/pictures" component = {Pictures}/>
	    		<Route path = "/projectsPage" component = {ProjectsPage}/>
              		<Route path = "/infoCardsPage" component = {InfoCardsPage}/>
                        <Route path = "/addProject" component = {AddProject}/>
          		<Route path = "/contractors" component = {Contractors}/>
	    		{/*login page causes a warning, not sure why, but it renders*/}
          		<Route path = "/login" component = {Login}/>
        	    	<Route path = "/createAccount" component = {CreateAccount}/>
	    		<Route path = "/projectInfo" component = {ProjectInfo}/>
      	    	</div>
        	</Router>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
