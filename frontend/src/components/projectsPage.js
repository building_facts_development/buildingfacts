import React, {Component} from 'react';
import './projectsPage.css';
import Layout from './layout.js'
import Projects from './projects.js'

export default class ProjectsPage extends Component{
	render(){
		return(
			<div>
				<Layout>
					<Projects/>
				</Layout>
			</div>
		);
	}
}
