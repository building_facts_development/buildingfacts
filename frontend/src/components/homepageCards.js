import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Logo from './../../static/images/commarch.svg';

import {
  Grid, Paper, Button, CardMedia
} from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 325,
    width: 325
  },
  control: {
    padding: theme.spacing.unit * 2
  },
  media: {
    height: 325,
  },
})

function HomeCard (props) {
  const { classes } = props;

  return (
    <Grid container spacing={16}>
      <Grid item xs={12}>
        <Grid container justify="center" spacing={40}>
            <Grid key={0} item>
              <Paper raised square className={classes.paper} >
                <CardMedia
                 className={classes.media}
                 image= {require("./../../static/images/box(multi).svg")}
                 title="Projects"
                 />
              </Paper >
            </Grid>
            <Grid key={1} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}
                 image= {require("./../../static/images/folder(g).svg")}
                 title="Contractors"
                 />
              </Paper >
            </Grid>
            <Grid key={2} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}
                 image= {require("./../../static/images/contractor(g).svg")}
                 title="Contractors"
                 />
              </Paper >
            </Grid>
            <Grid key={3} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}

                 image= {require("./../../static/images/pic(g).svg")}

                 title="Pictures"
                 />
              </Paper >
            </Grid>
        </Grid>
        <Grid container justify="center" spacing={40}>
            <Grid key={0} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}

                 image= {require("./../../static/images/icon-vrtour.svg")}

                 title="Pictures"
                 />
              </Paper >
            </Grid>
            <Grid key={1} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}

                 image= {require("./../../static/images/doc(g).svg")}

                 title="Projects"
                 />
              </Paper >
            </Grid>
            <Grid key={2} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}

                 image= {require("./../../static/images/map(g).svg")}

                 title="Contractors"
                 />
              </Paper >
            </Grid>
            <Grid key={3} item>
              <Paper square className={classes.paper} >
                <CardMedia
                 className={classes.media}

                 image= {require("./../../static/images/drone(g).svg")}

                 title="Pictures"
                 />
              </Paper >
            </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

HomeCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomeCard);
