import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  Grid, Paper, Button, Typography,
  TextField, MenuItem, Table, TableRow,
  TableBody, TableCell, TableHead,
  TableContent, Toolbar, InputLabel,
  Checkbox, Select, Tooltip,
  IconButton
} from '@material-ui/core';

const styles = theme => ({
  container: {
    display: 'table',
    height: '300px',
    width: '400px',
    padding: '16px'
  },
  grid: {
    marginLeft: '25px'
  },
  gridItem: {
    marginTop: '32px',
    marginLeft: '25px'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
  table: {
    width: 400,
  },
  button: {
    padding: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    margin: '16px'
  },
  header: {
    fontSize: '1rem'
  }
});

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

const tableContent = [];

let id = 0;
function createData(name, simpleName){
  id += 1;
  return {id, name, simpleName};
}

function pushData(data){
    tableContent.push(data);
}

let DynamicToolbar = props => {
  const {numSelected, selected, deleteSelected, classes} = props;

  return (
    <Toolbar className={classNames(classes.root, {[classes.highlight]: numSelected > 0})}>
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color='inherit' variant= 'subtitle1'>
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant='h6'>
            Sections
          </Typography>
        )}
      </div>
      <div className={classes.spacer}/>
      <div className={classes.actions}>
        {numSelected > 0 && (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete" onClick={() => deleteSelected()}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  )
}

DynamicToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
};

DynamicToolbar = withStyles(toolbarStyles)(DynamicToolbar);


class Section extends React.Component {

  state = {
    sectionName: '',
    sectionSimpleName: '',
    selected: [],
  }

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  deleteSelected = () => {
    var selected= [...this.state.selected]

    for (var i = 0; i < selected.length; i++) {
        tableContent.splice(tableContent.indexOf(selected[i]), 1)
    }
    selected.splice(0, selected.length)

    this.setState({ selected: selected})
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  handleTextFieldChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleAddButton = name => event => {
    pushData(
        createData(
          this.state.sectionName,
          this.state.sectionSimpleName,
    ));

    this.setState({
      sectionName: '',
      sectionSimpleName: '',
    })
  };

  render(){
    const { classes } = this.props
    const { selected } = this.state

    return (
      <div>
        <Grid className={classes.grid} container wrap="nowrap" direction="row">
          <Grid item >
            <DynamicToolbar numSelected={selected.length} selected={selected} deleteSelected={this.deleteSelected}/>

            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.header} variant='head'></TableCell>
                  <TableCell className={classes.header} variant='head'>Name</TableCell>
                  <TableCell className={classes.header} variant='head'>Simple Name</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableContent.map(row => {
                  const isSelected = this.isSelected(row.id)
                  return (
                  <TableRow
                    hover
                    key={row.id}
                    role='checkbox'
                    onClick={event => this.handleClick(event, row.id)}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.simpleName}
                    </TableCell>
                  </TableRow>
                )})}
              </TableBody>
            </Table>
          </Grid>

          <Grid item>
            <form className={classes.container} padding='8px' noValidate autoComplete="off">
              <Grid className={classes.gridItem} container direction="column" spacing={0}>
                <Grid item>
                  <TextField
                    id="sectionName"
                    label="Section Name"
                    value= {this.state.sectionName}
                    onChange={this.handleTextFieldChange("sectionName")}
                    className={classes.textField}
                    margin="normal"
                    variant="filled"
                    width="100%"
                    />
                </Grid>
                <Grid item>
                  <TextField
                    id="sectionSimpleName"
                    value= {this.state.sectionSimpleName}
                    onChange={this.handleTextFieldChange("sectionSimpleName")}
                    label="Simple Name"
                    className={classes.textField}
                    margin="normal"
                    variant="filled"
                    />
                </Grid>
              </Grid>
            </form>
          </Grid>
        </Grid>
        <div>
          <Button
            className={classes.button}
            variant='contained'
            onClick={(this.handleAddButton())}
            variant='contained'
            color="primary">
            Add
          </Button>
        </div>
      </div>
    )
  }
}


Section.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Section);
