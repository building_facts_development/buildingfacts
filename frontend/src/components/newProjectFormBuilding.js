import React from 'react';
import PropTypes from 'prop-types';
import './newProjectFormBuilding';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  Grid, Paper, Button, IconButton, Typography, Checkbox, TextField,
  MenuItem, Table, TableRow, TableBody, TableCell,
  TableHead, TableContent, Toolbar, Tooltip, Select, InputLabel
} from '@material-ui/core';

const styles = theme => ({
  container: {
    display: 'table',
    width: '100%',
    padding: '16px'
  },
  grid: {
    marginLeft: '25px'
  },
  gridItem: {
    marginTop: '32px',
    marginLeft: '25px'
  },
  textField: {
    paddingRight: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  textFieldLong: {
    width: '300px',
    paddingRight: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  TopSpacedTextField: {
    width: '300px',
    marginTop: '16px',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  Select: {
    height: '64px',
    width:'150px',
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
  table: {
    width: 400,

  },
  button: {
    padding: theme.spacing.unit,
    margin: '16px',
    marginBottom: theme.spacing.unit,
  },
  header: {
    fontSize: '1rem'
  }
});

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

let id = 0;
function createData(name, simpleName, classification, description, address, city, state, zipCode, startDate, endDate){
  id += 1;
  return {id, name, simpleName,
    classification, description, address,
    city, state, zipCode, startDate, endDate
  };
}

const tableContent = [];
const states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
                'Colorado', 'Connecticut', 'Delaware', 'Florida',
                'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana',
                'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
                'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
                'Mississippi', 'Missouri', 'Montana', 'Nebraska',
                'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
                'New York', 'North Carolina', 'North Dakota', 'Ohio',
                'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
                'South Carolina', 'South Dakota', 'Tennessee', 'Texas',
                'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia',
                'Wisconsin', 'Wyoming']

function pushData(data){
  tableContent.push(data);
}

let DynamicToolbar = props => {
  const {numSelected, selected, deleteSelected, classes} = props;

  return (
    <Toolbar className={classNames(classes.root, {[classes.highlight]: numSelected > 0})}>
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color='inherit' variant= 'subtitle1'>
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant='h6'>
            Buildings
          </Typography>
        )}
      </div>
      <div className={classes.spacer}/>
      <div className={classes.actions}>
        {numSelected > 0 && (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete" onClick={() => deleteSelected()}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        )}
      </div>
    </Toolbar>
  )
}

DynamicToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
};

DynamicToolbar = withStyles(toolbarStyles)(DynamicToolbar);


class Building extends React.Component {

  state = {
    buildingName: '',
    buildingSimpleName: '',
    buildingClassification: '',
    buildingDescription: '',
    buildingAddress: '',
    buildingCity: '',
    buildingState: '',
    buildingZipCode: '',
    buildingStartDate: '',
    buildingEndDate: '',
    selected: [],
  }

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  deleteSelected = () => {
    var selected= [...this.state.selected]

    for (var i = 0; i < selected.length; i++) {
        tableContent.splice(tableContent.indexOf(selected[i]), 1)
    }
    selected.splice(0, selected.length)

    this.setState({ selected: selected})
  }

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  handleTextFieldChange = name => event => {
      this.setState({ [name]: event.target.value});
  };

  handleStateSelectChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleAddButton = name => event => {
    pushData(
        createData(
          this.state.buildingName,
          this.state.buildingSimpleName,
          this.state.buildingClassification,
          this.state.buildingDescription,
          this.state.buildingAddress,
          this.state.buildingCity,
          this.state.buildingState,
          this.state.buildingZipCode,
          this.state.buildingStartDate,
          this.state.buildingEndDate,
        )
    );

    this.setState({
      buildingName: '',
      buildingSimpleName: '',
      buildingDescription: '',
      buildingClassification: '',
      buildingAddress: '',
      buildingCity: '',
      buildingState: '',
      buildingZipCode: '',
      buildingStartDate: '',
      buildingEndDate: '',
    })
  };

  render(){
    const { classes } = this.props
    const { selected } = this.state

    return (
      <div>
        <Grid className={classes.grid} container wrap="nowrap" direction="row">
          <Grid item >
            <DynamicToolbar numSelected={selected.length} selected={selected} deleteSelected={this.deleteSelected}/>

            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.header} variant='head'></TableCell>
                  <TableCell className={classes.header} variant='head'>Name</TableCell>
                  <TableCell className={classes.header} variant='head'>Simple Name</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableContent.map(row => {
                  const isSelected = this.isSelected(row.id)
                  return (
                    <TableRow
                      hover
                      key={row.id}
                      role='checkbox'
                      onClick= {event => this.handleClick(event, row.id)}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                          <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {row.simpleName}
                      </TableCell>
                    </TableRow>
                  )}
                )}
              </TableBody>
            </Table>
          </Grid>

          <Grid item>
            <form className={classes.container} padding='8px' noValidate autoComplete="off">
              <Grid className={classes.gridItem} container direction="column">
                <Grid item>
                  <TextField
                    id="buildingName"
                    label="Name"
                    value= {this.state.buildingName}
                    onChange={this.handleTextFieldChange("buildingName")}
                    className={classes.textFieldLong}
                    margin="normal"
                    variant="filled"
                    width="100%"
                    />
                    <TextField
                      id="buildingSimpleName"
                      label="Simple Name"
                      value= {this.state.buildingSimpleName}
                      onChange={this.handleTextFieldChange("buildingSimpleName")}
                      className={classes.textField}
                      margin="normal"
                      variant="filled"
                      />
                    <TextField
                      id="buildingClassification"
                      label="Classification"
                      value= {this.state.buildingClassification}
                      onChange={this.handleTextFieldChange("buildingClassification")}
                      className={classes.textField}
                      margin="normal"
                      variant="filled"
                      />
                </Grid>
                <Grid item>
                  <TextField
                    id="buildingDescription"
                    multiline
                    rows='4'
                    fullWidth
                    label="Description"
                    value= {this.state.buildingDescription}
                    onChange={this.handleTextFieldChange("buildingDescription")}
                    className={classes.textField}
                    margin="normal"
                    variant="filled"
                    />
                </Grid>
                <Grid item container direction='column'>
                  <Grid item>
                    <TextField
                    id="buildingAddress"
                    label="Address"
                    value= {this.state.buildingAddress}
                    onChange={this.handleTextFieldChange("buildingAddress")}
                    className={classes.TopSpacedTextField}
                    margin="normal"
                    variant="filled"
                    />
                  </Grid>
                  <Grid item>
                    <TextField
                      id="buildingCity"
                      label="City"
                      value= {this.state.buildingCity}
                      onChange={this.handleTextFieldChange("buildingCity")}
                      className={classes.textField}
                      margin="normal"
                      variant="filled"
                      />
                    <Select
                      id="buildingState"
                      label="State"
                      displayEmpty
                      value= {this.state.buildingState}
                      onChange={this.handleStateSelectChange}
                      className={classes.Select}
                      inputProps={{
                        name: 'buildingState',
                      }}
                      variant="filled"
                      >
                      <MenuItem value="" disabled>
                        <em>State</em>
                      </MenuItem>
                    {
                        states.map( state => (
                          <MenuItem value={state}>
                            {state}
                          </MenuItem>
                      ))}
                    </Select>
                    <TextField
                      id="buildingZipCode"
                      label="Zip Code"
                      value= {this.state.buildingZipCode}
                      onChange={this.handleTextFieldChange("buildingZipCode")}
                      className={classes.textField}
                      margin="normal"
                      variant="filled"
                      />
                  </Grid>
                </Grid>
                <Grid item container direction= "row">
                  <Grid item>
                    <TextField
                      id="buildingStartDate"
                      label="Start Date"
                      value= {this.state.buildingStartDate}
                      onChange={this.handleTextFieldChange("buildingStartDate")}
                      className={classes.textField}
                      margin="normal"
                      variant="filled"
                      />
                  </Grid>
                  <Grid item>
                    <TextField
                      id="buildingEndDate"
                      label="End Date"
                      value= {this.state.buildingEndDate}
                      onChange={this.handleTextFieldChange("buildingEndDate")}
                      className={classes.textField}
                      margin="normal"
                      variant="filled"
                      />
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </Grid>
          </Grid>
          <div>
            <Button
              className={classes.button}
              onClick={(this.handleAddButton())}
              variant='contained'
              color="primary"
              >
              add
            </Button>
          </div>
        </div>
    )
  }
}


Building.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Building);
