import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import './contractors.css';
import Layout from './layout.js';
import ContractorCard from './contractorCard.js';
import {
  Grid, Paper, Button, CardMedia, Card, CardContent, Typography, Divider
} from '@material-ui/core';

const styles = theme => ({
  grid: {
    margin: '16px',
    marginLeft: '25px'
  },
  title: {
    marginLeft: '100px',
    marginTop: '25px',
    maxWidth: '1350px'
  },
})

const contactLabels = [
  'Name:',
  'Office Address',
  'Phone 1',
  'Phone 2',
  'Fax',
  'Website',
  'Contacts',
  'Title',
  'Phone',
  'Email',
]

class Contractors extends Component{
  render(){
    const {classes} = this.props;

    return(
      <Layout>
        <Grid container className={classes.title} direction='column'>
          <Grid item>
            <Typography variant='h4' noWrap>
              General Contractor
            </Typography>
          </Grid>

          <Grid container className={classes.grid} spacing={32}>
            <Grid item xs={4}>
              <ContractorCard
                name="John Doe"
                officeAddress='102 Alumni Arena'
                phone1= 'xxx-xxx-xxxx'
                phone2= 'xxx-xxx-xxxx'
                fax= 'xxx-xxx-xxxx'
                website= 'johndoe.com'
                contacts= 'Assistant'
                title='contractor'
                contactPhone= 'xxx-xxx-xxxx'
                email= 'example@gmail.com'
                />
            </Grid>
          </Grid>

          <Grid item>
            <Typography variant='h4' noWrap>
              Architect
            </Typography>
          </Grid>

          <Grid container className={classes.grid} spacing={32}>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 1" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 2" title='contractor'/>
            </Grid>
          </Grid>

          <Grid item>
            <Typography variant='h4' noWrap>
              Mechanical Engineer
            </Typography>
          </Grid>

          <Grid container className={classes.grid} spacing={32}>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 1" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 2" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 3" title='contractor'/>
            </Grid>
          </Grid>

          <Grid item>
            <Typography variant='h4' noWrap>
              Electrical Engineer
            </Typography>
          </Grid>

          <Grid container className={classes.grid} spacing={32}>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 1" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 2" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 3" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 4" title='contractor'/>
            </Grid>
          </Grid>

          <Grid item>
            <Typography variant='h4' noWrap>
              Structural Engineer
            </Typography>
          </Grid>

          <Grid container className={classes.grid} spacing={32}>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 1" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 2" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 3" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 4" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 5" title='contractor'/>
            </Grid>
            <Grid item xs={4}>
              <ContractorCard name="Dummy 6" title='contractor'/>
            </Grid>
          </Grid>
        </Grid>
      </Layout>
    );
  }
}

Contractors.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Contractors);
