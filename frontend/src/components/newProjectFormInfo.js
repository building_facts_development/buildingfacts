import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid, Paper, Button,
  Typography, TextField, MenuItem,
  Select
} from '@material-ui/core';

const styles = theme => ({
  container: {
    display: 'table',
    flexWrap: 'wrap',
    width: '100%',
    padding: '16px'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  projectTextField: {
    width: '300px',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  AddressTextField: {
    width: '300px',
    marginTop: '35px',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  DescriptonTextField: {
    width: '450px',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
  Select: {
    height: '64px',
    width:'150px',
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

const states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
                'Colorado', 'Connecticut', 'Delaware', 'Florida',
                'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana',
                'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
                'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
                'Mississippi', 'Missouri', 'Montana', 'Nebraska',
                'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
                'New York', 'North Carolina', 'North Dakota', 'Ohio',
                'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
                'South Carolina', 'South Dakota', 'Tennessee', 'Texas',
                'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia',
                'Wisconsin', 'Wyoming']

class Info extends React.Component {

  state = {
    projectName: '',
    projectSimpleName: '',
    projectAddress: '',
    projectDescription: '',
    projectCity: '',
    projectsState: '',
    projectZipCode: '',
  }

  handleTextFieldChange = name => event => {
    this.setState({ [name]: event.target.value });
  }

  handleStateSelectChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  render(){
    const {classes} = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <Grid className={classes.container} container direction="column" spacing={0}>
          <Grid item>
            <TextField
              id="projectName"
              label="Project Name"
              className={classes.projectTextField}
              value= {this.state.projectName}
              onChange={this.handleTextFieldChange("projectName")}
              margin="normal"
              variant="filled"
              width="100%"
              />
            <TextField
              id="projectSimpleName"
              label="Simple Name"
              value= {this.state.projectSimpleName}
              onChange={this.handleTextFieldChange('projectSimpleName')}
              className={classes.textField}
              margin="normal"
              variant="filled"
              />
          </Grid>
          <Grid item>
            <TextField
              id="projectDescription"
              label="Project Descripton"
              value= {this.state.projectDescription}
              onChange={this.handleTextFieldChange('projectDescription')}
              className={classes.DescriptonTextField}
              rows='5'
              rowsMax='8'
              margin="normal"
              variant='filled'
              multiline
              />
          </Grid>
          <Grid item container direction='column'>
            <Grid item>
              <TextField
              id="projectAddress"
              label="Address"
              value= {this.state.projectAddress}
              onChange={this.handleTextFieldChange("projectAddress")}
              className={classes.AddressTextField}
              margin="normal"
              variant="filled"
              />
            </Grid>
            <Grid item>
              <TextField
                id="projectCity"
                label="City"
                value= {this.state.projectCity}
                onChange={this.handleTextFieldChange("projectCity")}
                className={classes.textField}
                margin="normal"
                variant="filled"
                />
              <Select
                id="projectsState"
                label="State"
                displayEmpty
                value= {this.state.projectsState}
                onChange={this.handleStateSelectChange}
                className={classes.Select}
                inputProps={{
                  name: 'projectsState',
                }}
              >
                <MenuItem value="" disabled>
                  <em>State</em>
                </MenuItem>
              {
                  states.map( state => (
                    <MenuItem value={state}>
                      {state}
                    </MenuItem>
                ))}
              </Select>
              <TextField
                id="projectZipCode"
                label="ZipCode"
                value= {this.state.projectZipCode}
                onChange={this.handleTextFieldChange("projectZipCode")}
                className={classes.textField}
                margin="normal"
                variant="filled"
                />
            </Grid>
          </Grid>
        </Grid>
      </form>
    )
  }
}


Info.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Info);
