import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import Button from '@material-ui/core/Button';
import SvgIcon from '@material-ui/core/SvgIcon';
import {Link} from 'react-router-dom';
import AddCircle from '@material-ui/icons/AddCircle';
import PersonAdd from '@material-ui/icons/PersonAdd';
import AccountBox from '@material-ui/icons/AccountBox';
import {green} from '@material-ui/core/colors';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: '240px',
    width: `calc(100% - ${240}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: '240px',
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: '240px',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 8 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    marginTop: '64px',
    height: '100vh',
    backgroundColor: green[100],
  },
  typography: {
    padding: theme.spacing.unit,
    color: "white",
  },
  quickHome: {
    padding: '0vw',
    marginLeft: 'auto',
    marginRight: '30px'
  },
});



function BoxIcon (props) {
  return (
    <SvgIcon  viewBox="0 0 503.65 503.64"{...props}>
    	<path d="M329.6,114.18c24.66,13.93,49,28.85,72.63,44.42L251.8,243.43l-150.66-85Q136.9,135,174.47,113.85C199.8,99.66,225.9,86.26,252.11,74q39.32,18.62,77.48,40.15"/>
	<path  d="M93.59,251.4c.08-26.16,1-52.6,2.81-78.63l148,83.49V429.12q-38-19.6-74.91-42c-24.9-15.14-49.41-31.34-73.13-48.2q-3-43.55-2.85-87.5"/>
	<path d="M259.22,256.28l148.23-83.62q2.63,39.48,2.63,79.3c0,30.28-1.15,60.78-3.4,90.77q-37.79,25-77.61,47.34c-22.82,12.75-46.22,24.93-69.86,36.18Z"/>
    </SvgIcon>
  );
}

function ListItemWLink(props) {
  const { primary, to, icon } = props;
  return (
    <ListItem button component={Link} to={to}>
      <ListItemIcon>
        {icon}
      </ListItemIcon>
      <ListItemText inset primary={primary} />
    </ListItem>
  );
}



class Layout extends React.Component {
  constructor(props){
  	super(props);
	this.state = {open: false};
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: this.state.open,
          })}
        >
          <Toolbar disableGutters={!this.state.open}>
	    <Button
	     color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(classes.menuButton, {
                [classes.hide]: this.state.open,
              })}
	    >
	    <Typography varient = "p" className = {classes.typography}>
	    	Menu
	    </Typography>
	    <MenuIcon/>
	    </Button>
	    <a className = {classes.quickHome}  href = '/'>
          <img src={require('./../../static/images/BFarch(w)1.svg')} height = "65vh"/>
          </a>
	  </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: this.state.open,
            [classes.drawerClose]: !this.state.open,
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            }),
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <List>
      <ListItemWLink primary="Projects" to="/projectsPage" icon={<BoxIcon/>} />
	    <ListItemWLink primary="Add Project" to="/addProject" icon={<AddCircle/>} />
      <ListItemWLink primary="Add User" to="/CreateAccount" icon={<PersonAdd/>} />
      <ListItemWLink primary="Account" to="/" icon={<AccountBox/>} />
	    <Divider/>
          </List>
        </Drawer>
	<main className={classes.content}>
        {this.props.children}
      </main>
      </div>
    );
  }
}

Layout.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Layout);
