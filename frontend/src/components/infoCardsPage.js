import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Layout from './layout.js';
import InfoCards from './infoCards.js';


export default class InfoCardsPage extends Component {
  render(){
    return(
      <Layout>  
        <InfoCards />
      </Layout>
    );
  } 
}

