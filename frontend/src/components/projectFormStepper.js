import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {Stepper, Step, StepButton, Paper, Button, Typography} from '@material-ui/core/';
import Info from './newProjectFormInfo.js'
import Section from './newProjectFormSection.js'
import Building from './newProjectFormBuilding.js'
import Floor from './newProjectFormFloor.js'

const styles = theme => ({
  root: {
    width: '80%',
    marginLeft: '24px',
  },
  button: {
    margin: '16px',
    padding: theme.spacing.unit,
  },
  nextButton: {
    margin: '16px',
    marginLeft: '76%',
    padding: theme.spacing.unit,
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
});

function getSteps(){
  return ["Project Info", "Section", "Building Info", "Floor Info"];
}

var fieldValues = {
  projectName: '',
  projectAddress: '',
  projectSimpleName: '',
  projectDescription: '',

  sections: [],
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <Info
              fieldValues={fieldValues}
              />
    case 1:
      return <Section
              fieldValues={fieldValues}
              />
    case 2:
      return <Building/>
    case 3:
      return <Floor/>
    default:
      return 'Unknown step';
  }
}

class ProjectStepper extends React.Component {
  state = {
    activeStep: 0,
    completed: {},
  };

  totalSteps = () => getSteps().length;

  handleNext = () => {
    let activeStep;

    if (this.isLastStep() && !this.allStepsCompleted()) {
      // It's the last step, but not all steps have been completed,
      // find the first step that has been completed
      const steps = getSteps();
      activeStep = steps.findIndex((step, i) => !(i in this.state.completed));
    } else {
      activeStep = this.state.activeStep + 1;
    }
    this.setState({
      activeStep,
    });
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  handleStep = step => () => {
    this.setState({
      activeStep: step,
    });
  };

  handleComplete = () => {
    const { completed } = this.state;
    completed[this.state.activeStep] = true;
    this.setState({
      completed,
    });
    this.handleNext();
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
      completed: {},
    });
  };

  completedSteps() {
    return Object.keys(this.state.completed).length;
  }

  isLastStep() {
    return this.state.activeStep === this.totalSteps() - 1;
  }

  allStepsCompleted() {
    return this.completedSteps() === this.totalSteps();
  }

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <Paper className={classes.root}>
        <Stepper className='customStepper' nonLinear activeStep={activeStep}>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepButton disableRipple onClick={this.handleStep(index)} completed={this.state.completed[index]}>
                {label}
              </StepButton>
            </Step>
          ))}
        </Stepper>
        <div>
          {this.allStepsCompleted() ? (
            <div>
              <Typography className={classes.instructions}>
                All steps completed - you&apos;re finished
              </Typography>
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              <div>{getStepContent(activeStep)}</div>
              <div position='relative'>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.button}
                  variant='contained'
                  color="primary"
                >
                  Back
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleNext}
                  className={classes.nextButton}
                >
                  Next
                </Button>
                {activeStep !== steps.length &&
                  (this.state.completed[this.state.activeStep] ? (
                    <Typography variant="caption" className={classes.completed}>
                      Step {activeStep + 1} already completed
                    </Typography>
                  ) : (
                    <Button className={classes.button} variant="contained" color="primary" onClick={this.handleComplete}>
                      {this.completedSteps() === this.totalSteps() - 1 ? 'Finish' : 'Complete Step'}
                    </Button>
                  ))}
              </div>
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

ProjectStepper.propTypes = {
  classes: PropTypes.object,
};

export default withStyles(styles)(ProjectStepper);
