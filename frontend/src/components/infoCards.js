import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ErrorBoundary from './errorBoundary.js';

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: 250,
  },
  media: {
    objectFit: 'cover',
  },
  cardAction: {
    display: 'block',
    textAlign: 'initial',
  },
};

function InfoCards(props) {
  const { classes } = props;

  function handleClick(e) {
    const {link} = props;
    e.preventDefault();
    let path = {link};
    props.history.push(path);
  }


  return ( 
    <ErrorBoundary>
      <Grid container justify="center" spacing={24} alignItems="center" >
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
                component="img"
                alt="Project Info Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/info(g).svg"
                title="Project Info"
	  	link = {"/projectInfo"}
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Project Info
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
	  	link = {"/contractors.js"}
                component="img"
                alt="Contractors Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/contractor(g).svg"
                title="Contractors"
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Contractors
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
                component="img"
                alt="Pictures Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/pic(g).svg"
                title="Pictures"
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Pictures
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
                component="img"
                alt="Map Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/map(g).svg"
                title="CommercialVR"
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Map
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
                component="img"
                alt="Documents Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/doc(g).svg"
                title="Documents"
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Documents
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
                component="img"
                alt="Aerial Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/drone(g).svg"
                title="Aerial"
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Aerial
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card} raised >
            <CardActionArea className={props.classes.cardAction} onClick={handleClick} >
              <CardMedia
                component="img"
                alt="Folders Logo"
                className={classes.media}
                height="250"
                image="./../../static/images/folder(g).svg"
                title="Folders"
              />
              <CardContent>
                <Typography gutterBottom variant="h4" component="h2" align="center" >
                  Folders
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    </ErrorBoundary>
  )
}

InfoCards.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InfoCards);
