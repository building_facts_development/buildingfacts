import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import {pushForm} from './functionalities.js';
const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', 
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: '75%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
  submit: {
    width: '25%',
    marginTop: theme.spacing.unit * 3,
  },
});

class CreateAcctForm extends Component {

constructor(props) {
  super(props);
  this.state = {
     username: '',
     email: '',
     fname: '',
     lname: '',
     company: '',
     title:'',  
  };
  this.handleSubmit = this.handleSubmit.bind(this);
}

handleChange = name => event => {
event.preventDefault();    
this.setState({ [name]: event.target.value });
  };

handleSubmit = event => {
event.preventDefault();
this.setState = ({
     username: '',
     email: '',
     fname: '',
     lname: '',
     company: '',
     title:'',
  });
pushForm(this.props);
}
render(){
const {classes} = this.props;
  return (
    <main className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
	<Typography component="h1" variant="h5">
           Add a New User 
        </Typography>
        <form className={classes.form} method = 'POST'>
	  <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="username">Username</InputLabel>
            <Input id="username" name="username" value = {this.state.username} 
	  	onChange={this.handleChange("username")} 
	  autoFocus />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">Email Address</InputLabel>
            <Input id="email" name="email" value = {this.state.email}
	  	onChange={this.handleChange("email")}
	  />
          </FormControl>
	  <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="fname">First Name</InputLabel>
            <Input id="fname" name="fname" value = {this.state.fname}
	  	onChange={this.handleChange("fname")}
	  />
          </FormControl>
	  <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="lname">Last Name</InputLabel>
            <Input id="lname" name="lname" value = {this.state.lname}
	  	onChange={this.handleChange("lname")}
	  />
          </FormControl>
	  <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="company">Company</InputLabel>
            <Input name="company" id="company" value = {this.state.company}
	  	onChange={this.handleChange("company")}
	  />
          </FormControl>
	  <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="title">Title</InputLabel>
            <Input id="title" name="title" value = {this.state.title} 
	  	onChange={this.handleChange("title")}
	  />
          </FormControl>
	  <br/>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
	    onSubmit = {this.handleSubmit.bind(this)}
          >
            Create User
          </Button>
        </form>
      </Paper>
    </main>
  );
}	
}
CreateAcctForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateAcctForm);

