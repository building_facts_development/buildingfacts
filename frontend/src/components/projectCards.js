import React, { Component } from 'react';
import './projects.css';
import { Link, render, withRouter } from 'react-router-dom';
import Layout from './layout.js';
import ErrorBoundary from './errorBoundary.js';
import InfoCardsPage from './infoCardsPage.js';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';


const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    objectFit: 'cover',
  },
  cardAction: {
    display: 'block',
    textAlign: 'initial',
  },
};

function ProjectCards(props) {
  const { classes } = props;

  function handleClick(e) {
    e.preventDefault();
    let path = './infoCardsPage';
    props.history.push(path);
  }

  return (
    <ErrorBoundary>
      <Card className={classes.card} raised >
        <CardActionArea className={props.classes.cardAction} onClick={handleClick}>
          <CardMedia
            component="img"
            alt="Construction Image"
            className={classes.media}
            height="140"
            image="http://firstsynergiconstruction.com/wp-content/uploads/2015/02/IMG-20160718-WA003.jpg"
            title="Construction Image"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              Project XYZ
            </Typography>
            <Typography component="p">
              This is where you would place descriptive text if you choose.
              This text can be anything you want. 
              Maybe a date of some sort.
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </ErrorBoundary>
  );
}

ProjectCards.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter( withStyles(styles)(ProjectCards) );
