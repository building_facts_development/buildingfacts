import React, { Component } from 'react';
import './projects.css';
import { Link, render } from 'react-router-dom';
import Layout from './layout.js';
import ProjectCards from './projectCards.js';

import { 
  MDBContainer,
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBRow,
  MDBCol, 
  MDBView,
  MDBMask }  from 'mdbreact';


const Projects = () => {
  return (
    <MDBContainer fluid>
      <MDBCarousel activeItem={1} length={2} showControls={true} showIndicators={true} interval={1000000}>
        <MDBCarouselInner>

          <MDBCarouselItem itemId="1">
            <MDBContainer className="w-75 text-center mx-auto p-5 mb-4" fluid>     
              <MDBRow className="mb-4">
                <MDBCol>
                  <ProjectCards/>
                </MDBCol>
                <MDBCol>
                  <ProjectCards/>
                </MDBCol>
                <MDBCol>
                  <ProjectCards/>
                </MDBCol>
              </MDBRow>
              <MDBRow>
                <MDBCol>
                  <ProjectCards/>
                </MDBCol>
                <MDBCol>
                  <ProjectCards/>
                </MDBCol>
                <MDBCol>
                  <ProjectCards/>
                </MDBCol>
              </MDBRow> 
            </MDBContainer>
          </MDBCarouselItem>
 
           <MDBCarouselItem itemId="2">
            <MDBContainer className="w-75 text-center mx-auto p-5 mb-4" fluid>
                <MDBRow className="mb-4">
                  <MDBCol>
                    <ProjectCards/>
                  </MDBCol>
                  <MDBCol>
                    <ProjectCards/>
                  </MDBCol>
                  <MDBCol>
                    <ProjectCards/>
                  </MDBCol>
                </MDBRow>
                <MDBRow>
                  <MDBCol>
                    <ProjectCards/>
                  </MDBCol>
                  <MDBCol>
                   <ProjectCards/>
                  </MDBCol>
                  <MDBCol>
                   <ProjectCards/>
                  </MDBCol>
                </MDBRow>            
            </MDBContainer>
          </MDBCarouselItem>

        </MDBCarouselInner>
      </MDBCarousel>
    </MDBContainer>
  );
}

export default Projects;
