import fetch from 'isomorphic-fetch';
import {Link, Redirect} from 'react-router-dom';
import axios from 'axios';
import React from 'react';

export function pushForm(data){
        return fetch({
                method: 'POST',
                mode: 'CORS',
                username: JSON.stringify(data),
		email: JSON.stringify(data),
		fname: JSON.stringify(data),
                lname: JSON.stringify(data),
		company: JSON.stringify(data),
		title: JSON.stringify(data),
		headers: {
                        'Content-Type' : 'application/json'
                }
        }).then( res=> {
                        return res;
        }).catch(err => err);
	const user = {
		username: data.username,
		email: data.email,
	    	fname: data.fname,
            	lname: data.lname,
		company: data.company,
		title: data.title,
        };
        axios.post('https://jsonplaceholder.typicode.com/users', { user })
            .then(res => {
                alert("Account created");
                console.log(res);
                console.log(res.data);
            })

}
export function pushLogin(data){
    const attempt = {
            username: data.username,
            password: data.password
        };
        axios.post('https://jsonplaceholder.typicode.com/users', { attempt })
           .then(res => {
                console.log(res);
                 console.log(res.data);
            })

}


export function requireAuth(){

}
export const FormErrors = ({formErrors}) =>
  <div className='formErrors'>
    {Object.keys(formErrors).map((fieldName, i) => {
      if(formErrors[fieldName].length > 0){
        return (
          <p key={i}>{fieldName} {formErrors[fieldName]}</p>
        )
      } else {
        return '';
      }
    })}
</div>
