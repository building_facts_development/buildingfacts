import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import {Card, CardHeader, CardMedia, CardContent,
  CardActions, Collapse, IconButton, Typography, Grid} from '@material-ui/core/';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  card: {
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});

class ContractorCard extends Component {
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes } = this.props;

    return (
      <Card className={classes.card}>
      <Grid container alignItems='baseline' justify='space-between' direction='row'>
          <Grid item>
            <CardHeader
              title= {this.props.name}
              subheader= {this.props.title}
            />
          </Grid>
          <Grid item>
            <IconButton
              className={classNames(classes.expand, {
                [classes.expandOpen]: this.state.expanded,
              })}
              onClick={this.handleExpandClick}
              aria-expanded={this.state.expanded}
              aria-label="Show info"
              color='primary'
            >
              <ExpandMoreIcon />
            </IconButton>
          </Grid>
        </Grid>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Grid container direction='row' spacing={16}>
              <Grid item>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Name: {this.props.name}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Office Address: {this.props.officeAddress}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Phone 1: {this.props.phone1}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Phone 2: {this.props.phone2}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Fax: {this.props.fax}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Website: {this.props.website}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Contact: {this.props.contacts}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Title: {this.props.title}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Phone: {this.props.contactPhone}
                </Typography>
                <Typography variant='subtitle1' gutterBottom noWrap>
                  Email: {this.props.email}
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}

ContractorCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContractorCard);
