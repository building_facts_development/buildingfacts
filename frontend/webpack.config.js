var webpack = require('webpack');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        path: __dirname,
        filename: 'static/js/bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-react']
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|tff|svg)$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.json$/,
                loader:'json-loader'
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'static/css/styles.min.css'
        })
    ],
    devServer: {
        contentBase: [
            './public',
            './'
        ],
        historyApiFallback: true
    }
};

