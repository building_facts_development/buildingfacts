# Building Facts

For any given room in a building, there is an interactive image with buttons on it that have  attached PDF detailing 
the appliance related to that button. This will make knowing what the room you're getting yourself into much easier. 
There is also a section for blueprints which can  save service workers a ton of time and  effort (and money) when making 
repairs. Both of these items combine to make an effective tool to see EXACTLY what you're buying and for incredibly easy maintenance.

The impact of this website is that maintenence companies and apartment owners will save money from the correct implementation of
this website! Both of these companies have to put money into repairing things and if the exact location of where items that need repair
are known down to the inch, then both parties have to buy minimal materials. Renters benefit as well! When they want to rent an apartment, 
they can click the link to the building facts site and see literally everything there is to see about that complex, down to the paint thats on the wall!

Meant to be implemented using a python back end and a JavaScript front end (ReactJS, Django, Postgres).

Scrum board: https://trello.com/b/otKCuEgg/cse442

This project is an update to an already existing website and already existing legacy code. 
Parent Site:
[BuildingFacts](http://www.buildingfacts.com/)
Sample of the website that we're fixing:
[BuildingFacts Sample](http://sample.buildingfacts.com/)

# SETUP INSTRUCTIONS

Make sure to read the README for the frontend and the backend before running these scripts if you are developing.

## One-time Installation

Follow instructions on the [Docker Website](https://docs.docker.com/install/) to install Docker on your respective system.

Also follow the instruction on the [Docker Compose Website](https://docs.docker.com/compose/install/) to install Docker Compose on to your respective system.

*Note that the server is currently configured to run on a Raspberry Pi.  Please read the README in frontend, backend, and nginx to make sure there are not any specific changes that need to be made to run the server on your own setup

## Initial Configuration

Execute all of the following commands from the project root after the repo is cloned.

```
mkdir config
mkdir data
mkdir data/media
mkdir data/postgres_data

touch config/backend.env
touch config/db.env
```

The `*.env` files found within the config directory will be used to generate the environment variables for their respective services.  Their primary use is to configure the connection from the backend to the database.

Use the following as a template (fields with `*****` will need to be configured to your own values.  Make sure that these values match between backend and db.  EX: SQL_USER in backend must match POSTGRES_USER in db)...

```
# config/backend.env
SECRET_KEY=*****
SQL_ENGINE=django.db.backends.postgresql
SQL_DATABASE=postgres
SQL_USER=*****
SQL_PASSWORD=*****
SQL_HOST=db
SQL_PORT=5432
DATABASE=postgres

# Uncomment the following line to allow for debug output in the browser
# DEBUG=1
```

```
# config/db.env
POSTGRES_PASSWORD=*****
POSTGRES_USER=*****
POSTGRES_DB=*****
PGDATA=/var/lib/postgresql/data
```


## Running the Server Stack

Very simple...

### If the server is not yet running

This will run build the server images and run the server docker images together.

```
docker-compose up
```

(Use the `-d` flag to run in the background.)

### If the server is already running

This will rebuild the server instance and replace the current running instance with the new instance.  Images that have not been changed will not be replaced.

```
docker-compose build
docker-compose up
```

In order to build only a specific service, use `docker-compose build <service>` where service is any from the following list.

- `frontend`
- `backend`
- `nginx`
- `db`

(Again, use the `d` flag on `docker-compose up` to run the server in the background.)

Now the website will be running on `127.0.0.1:1337`
