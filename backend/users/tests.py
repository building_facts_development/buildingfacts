import json

from datetime import datetime

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.test import Client, TestCase
from rest_framework.test import APIClient

from model.model_company import Company, CompanyClassification
from users.models import UserProfile


def setup_Company():
    cc = CompanyClassification(
            id_code = 'BC',
            name = 'BaseClassification'
    )

    cc.full_clean()
    cc.save()

    c = Company(
            name = 'BaseCompany',
            email = 'basecompany@email.com',
            address = '50 Base Company Dr',
            city = 'Basey',
            postal_code = '12345',
            phone = '1234567890'
    )
    c.full_clean()
    c.save()


def setup_User():
    usr = User.objects.create_superuser('testuser', 'user@company.com', 'password')

    profile = UserProfile(
            user = usr,
            first_name = 'first',
            last_name = 'last',
            title = 'CEO',
            email = 'email@company.com',
            phone_mobile = '7161234567',
            phone_office = '7161234567',
            fax = '7161234567',
            company = Company.objects.get(pk=1)
    )
    profile.full_clean()
    profile.save()


class UserProfileTest(TestCase):
    '''
        Tests that operate on the UserProfile model
    '''

    def setUp(self):
        setup_Company()
        setup_User()

    def test_full_name(self):
        '''
            The full name property returns correctly
        '''
        fn = UserProfile.objects.get(pk=1).full_name
        self.assertEqual(fn, 'first last')

    def test_onetoone_user(self):
        '''
            The User should have a one to one relationship with UserProfile
        '''
        try:
            u = UserProfile.objects.get(pk=1)
            u.pk = None
            u.full_clean()
            u.save()
        except ValidationError as e:
            self.assertEqual(len(e.message_dict), 1)
            self.assertTrue('user' in e.message_dict)
        

    def test_sorting(self):
        '''
            Sorting by lastname, then first name, then pk
        '''
        u = UserProfile.objects.get(pk=1)

        usr = User.objects.create_user('testuser1', 'user1@company.com', 'password')
        u.user = usr
        u.pk = None
        u.last_name = 'last'
        u.first_name = 'z'
        u.full_clean()
        u.save()

        usr = User.objects.create_user('testuser2', 'user2@company.com', 'password')
        u.user = usr
        u.pk = None
        u.last_name = 'last'
        u.first_name = 'first'
        u.full_clean()
        u.save()

        usr = User.objects.create_user('testuser3', 'user3@company.com', 'password')
        u.user = usr
        u.pk = None
        u.last_name = 'first'
        u.first_name = 'last'
        u.full_clean()
        u.save()

        self.assertEqual(UserProfile.objects.count(), 4)

        lst = UserProfile.objects.all()

        self.assertEqual(lst[0].pk, 4)
        self.assertEqual(lst[1].pk, 1)
        self.assertEqual(lst[2].pk, 3)
        self.assertEqual(lst[3].pk, 2)

class UsersTest(TestCase):
    
    c = APIClient(enforce_csrf_checks=True)
    logged_in = False

    valid_user1_get = {
            'id': 1,
            'username': 'testuser',
            'is_staff': True,
            'last_login': None,
            'profile': {
                'first_name': 'first',
                'last_name': 'last',
                'title': 'CEO',
                'email': 'email@company.com',
                'phone_mobile': '7161234567',
                'phone_office': '7161234567',
                'fax': '7161234567',
                'company': 1
            }
        }

    valid_user1_change = {
            'id': 1,
            'username': 'testuser',
            'is_staff': True,
            'last_login': None,
            'profile': {
                'first_name': 'firstname',
                'last_name': 'lastname',
                'title': 'CTO',
                'email': 'emails@company.com',
                'phone_mobile': '7171234567',
                'phone_office': '7171234567',
                'fax': '7171234567',
                'company': 1
            }
        }

    valid_user2_create = {
            'username': 'testuser2',
            'password': 'testpassword',
            'profile': {
                'first_name': 'first',
                'last_name': 'last',
                'title': 'CEO',
                'email': 'email@company.com',
                'phone_mobile': '7161234567',
                'phone_office': '7161234567',
                'fax': '7161234567',
                'company': 1
            }
        }

    valid_user2_get = {
            'id': 2,
            'username': 'testuser2',
            'is_staff': False,
            'last_login': None,
            'profile': {
                'first_name': 'first',
                'last_name': 'last',
                'title': 'CEO',
                'email': 'email@company.com',
                'phone_mobile': '7161234567',
                'phone_office': '7161234567',
                'fax': '7161234567',
                'company': 1
            }
        }

    invalid_user1 = {
            'user': 'testuser2',
            'pass': None,
            'profil': {
                'first_name': 'first',
                'last_name': 'last',
                'title': 'CEO',
                'email': 'email@company.com',
                'phone_mobile': '7161234567',
                'phone_office': '7161234567',
                'fax': '7161234567',
                'company': 1
            }
        }

    invalid_user2 = {
            'username': 'testuser2',
            'password': 'password',
            'profile': {
                'first': 'first',
                'last': 'last',
                'title_name': 'CEO',
                'personal_email': 'email@company.com',
                'phone_num': '7161234567',
                'phone_offic': '7161234567',
                'fax_num': '7161234567',
                'company': 'one'
            }
        }

    invalid_user3 = {'data':'{"a bunch of gibberish: that" has no" }} meaning {{[ at all"'}

    def setUp(self):
        setup_Company()
        setup_User()

    def test_user_no_auth(self):
        resp = self.c.get('/api/v1/users/')
        self.assertEqual(resp.status_code, 401)

        resp = self.c.get('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 401)

        resp = self.c.get('/api/v1/users/self/')
        self.assertEqual(resp.status_code, 401)

        resp = self.c.post('/api/v1/users/')
        self.assertEqual(resp.status_code, 401)

        resp = self.c.put('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 401)

        resp = self.c.delete('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 401)

    def test_user_list_get(self):
        self.login()

        resp = self.c.get('/api/v1/users/')
        self.assertEqual(resp.status_code, 200)
        lst = [self.valid_user1_get]

        self.assertEqual(resp.json(), lst)

    def test_user_list_post(self):
        self.login()

        resp = self.c.post('/api/v1/users/', self.valid_user2_create, format='json')
        self.assertEqual(resp.status_code, 201)

        self.assertEqual(resp.json(), self.valid_user2_get)

        resp = self.c.get('/api/v1/users/')
        self.assertEqual(resp.status_code, 200)

        lst = [self.valid_user1_get, self.valid_user2_get]

        self.assertEqual(resp.json(), lst)

    def test_user_list_post_invalid(self):
        self.login()

        resp = self.c.post('/api/v1/users/', self.invalid_user1, format='json')
        self.assertEqual(resp.status_code, 400)

        resp = self.c.post('/api/v1/users/', self.invalid_user2, format='json')
        self.assertEqual(resp.status_code, 400)

        resp = self.c.post('/api/v1/users/', self.invalid_user3)
        self.assertEqual(resp.status_code, 400)

        lst = [self.valid_user2_create]
        resp = self.c.post('/api/v1/users/', lst, format='json')
        self.assertEqual(resp.status_code, 400)

    def test_user_nonexistant(self):
        self.login()

        resp = self.c.get('/api/v1/users/2/')
        self.assertEqual(resp.status_code, 404)

        resp = self.c.put('/api/v1/users/2/', self.valid_user1_get, format='json')
        self.assertEqual(resp.status_code, 404)

        resp = self.c.delete('/api/v1/users/2/')
        self.assertEqual(resp.status_code, 404)

    def test_user_get(self):
        self.login()

        resp = self.c.get('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(resp.json(), self.valid_user1_get)

    def test_user_get_noauth(self):
        self.login()

        self.c.post('/api/v1/users/', self.valid_user2_create, format='json')

        self.login(user=2)

        resp = self.c.get('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 403)

    def test_user_put(self):
        self.login()

        changes = {'profile': self.valid_user1_change['profile']}

        resp = self.c.put('/api/v1/users/1/', changes, format='json')
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(resp.json(), self.valid_user1_change)

        resp = self.c.get('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(resp.json(), self.valid_user1_change)

    def test_user_put_invalid(self):
        self.login()

        resp = self.c.put('/api/v1/users/1/', self.invalid_user1, format='json')
        self.assertEqual(resp.status_code, 400)

        resp = self.c.put('/api/v1/users/1/', self.invalid_user2, format='json')
        self.assertEqual(resp.status_code, 400)

        resp = self.c.put('/api/v1/users/1/', self.invalid_user3)
        self.assertEqual(resp.status_code, 400)

        lst = [self.valid_user2_create]
        resp = self.c.put('/api/v1/users/1/', lst, format='json')
        self.assertEqual(resp.status_code, 400)

    def test_user_put_noauth(self):
        self.login()

        self.c.post('/api/v1/users/', self.valid_user2_create, format='json')

        self.login(user=2)

        changes = {'profile': self.valid_user1_change['profile']}

        resp = self.c.put('/api/v1/users/1/', changes, format='json')
        self.assertEqual(resp.status_code, 403)

    def test_user_delete(self):
        self.login()

        self.c.post('/api/v1/users/', self.valid_user2_create, format='json')

        resp = self.c.get('/api/v1/users/2/')
        self.assertEqual(resp.status_code, 200)

        resp = self.c.delete('/api/v1/users/2/')
        self.assertEqual(resp.status_code, 204)

        resp = self.c.get('/api/v1/users/2/')
        self.assertEqual(resp.status_code, 404)

    def test_user_delete_noauth(self):
        self.login()

        self.c.post('/api/v1/users/', self.valid_user2_create, format='json')

        self.login(user=2)

        resp = self.c.delete('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 403)

    def test_user_delete_self(self):
        self.login()

        resp = self.c.delete('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 400)

    def test_user_getself(self):
        self.login()

        resp = self.c.get('/api/v1/users/self/')
        self.assertEqual(resp.status_code, 200)

        self.assertEqual(resp.json(), self.valid_user1_get)

    def test_methods(self):
        self.login()

        resp = self.c.put('/api/v1/users/')
        self.assertEqual(resp.status_code, 405)

        resp = self.c.delete('/api/v1/users/')
        self.assertEqual(resp.status_code, 405)

        resp = self.c.post('/api/v1/users/1/')
        self.assertEqual(resp.status_code, 405)

        resp = self.c.post('/api/v1/users/self/')
        self.assertEqual(resp.status_code, 405)

        resp = self.c.put('/api/v1/users/self/')
        self.assertEqual(resp.status_code, 405)

        resp = self.c.delete('/api/v1/users/self/')
        self.assertEqual(resp.status_code, 405)

    def login(self, user=1):
        self.c.credentials()

        username = 'testuser'
        password = 'password'

        if user is 2:
            username = 'testuser2'
            password = 'testpassword'

        resp = self.c.post(
                '/api/v1/login/',
                {'username': username, 'password': password}
        )

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('token' in resp.json())

        self.c.credentials(HTTP_AUTHORIZATION='Token '+resp.json().get('token'))

