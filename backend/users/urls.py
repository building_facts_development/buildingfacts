from django.contrib import admin
from django.urls import path

from users.views import user_list, user_details, user_self


urlpatterns = [
    path('users/', user_list),
    path('users/<int:pk>/', user_details),
    path('users/self/', user_self),
]

