from django.db import models

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from model.model_company import Company
from model.model_util import ModelBase
from model.model_validators import phone_validator


class UserProfile(ModelBase):
    '''
        Database model for extra user profile information 
        beyond simple login information
    '''

    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=80)
    title = models.CharField(max_length=50, blank=True)
    email = models.EmailField(blank=True)
    phone_mobile = models.CharField(
        validators=[phone_validator],
        max_length=17,
        blank=True
    )
    phone_office = models.CharField(
        validators=[phone_validator],
        max_length=17,
        blank=True
    )
    fax = models.CharField(
        validators=[phone_validator],
        max_length=17,
        blank=True
    )
    company = models.ForeignKey(
        'model.Company',
        on_delete=models.CASCADE,
        related_name='users',
        related_query_name='user'
    )
    
    class Meta:
        ordering = ['last_name', 'first_name']

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'User profiles'


class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline,)

