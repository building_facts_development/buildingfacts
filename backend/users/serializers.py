from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework import serializers

from users.models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = (
            'first_name',
            'last_name',
            'title',
            'email',
            'phone_mobile',
            'phone_office',
            'fax',
            'company',
        )


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(source='userprofile', many=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'is_staff', 'last_login', 'profile')
        read_only_fields = ('id', 'username', 'is_staff', 'last_login')

    def create(self, validated_data):
        # Create should not be called
        return False

    def update(self, instance, validated_data):
        data = validated_data.pop('userprofile')

        profile = instance.userprofile

        profile.first_name = data.get('first_name', profile.first_name)
        profile.last_name = data.get('last_name', profile.last_name)
        profile.title = data.get('title', profile.title)
        profile.email = data.get('email', profile.email)
        profile.phone_mobile = data.get('phone_mobile', profile.phone_mobile)
        profile.phone_office = data.get('phone_office', profile.phone_office)
        profile.fax = data.get('fax', profile.fax)
        profile.company = data.get('company', profile.company)

        profile.save()
        return instance


class UserCreateSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(source='userprofile', many=False)

    class Meta:
        model = User
        fields = ('username', 'password', 'profile')

    def create(self, validated_data):
        profile_data = validated_data.pop('userprofile')
        username = validated_data.pop('username')
        password = validated_data.pop('password')

        user = get_user_model().objects.create_user(username, password=password)
        UserProfile.objects.create(user=user, **profile_data)

        return user

    def update(self, instance, validated_data):
        # Update should not be called!
        assert False


class UserUpdatePassword(serializers.Serializer):
    # TODO Add a serializer for updating user passwords
    pass 

