from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from users.serializers import UserSerializer, UserCreateSerializer


@csrf_exempt
@api_view(['GET', 'POST'])
def user_list(request):

    if request.method == 'GET':
        users = User.objects.all()
        serialize = UserSerializer(users, many=True)
        return JsonResponse(serialize.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serialize = UserCreateSerializer(data=data)

        if serialize.is_valid():
            serialize.save()

            username = serialize.data.pop('username')
            user = User.objects.get(username=username)
            user_serializer = UserSerializer(user)

            return JsonResponse(user_serializer.data, status=201)

        return JsonResponse(serialize.errors, status=400)


@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def user_details(request, pk):

    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return HttpResponse(status=404)

    # If the user is requesting themselves or is a staff, they can see
    # individual users
    if not request.user.is_staff:
        if user.pk is not request.user.pk:
            return HttpResponse(status=403)

    if request.method == 'GET':
        serialize = UserSerializer(user)
        return JsonResponse(serialize.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serialize = UserSerializer(user, data=data)

        if serialize.is_valid():
            serialize.save()
            return JsonResponse(serialize.data)

        return JsonResponse(serialize.errors, status=400)

    elif request.method == 'DELETE':
        if request.user.pk is user.pk:
            return JsonResponse({'error':'Can not delete own account'}, status=400)

        user.delete()
        return HttpResponse(status=204)


@csrf_exempt
@api_view(['GET'])
def user_self(request):
    '''
        Allows the logged in user to get their own profile
    '''

    user = request.user
    serialize = UserSerializer(user, many=False)
    return JsonResponse(serialize.data)


