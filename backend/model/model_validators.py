from django.core.validators import RegexValidator


phone_validator = RegexValidator(
    regex=r'^\+?1?\d{9,15}$',
    message='Phone number must be entered in the format: \'+999999999\'. Up to 15 digits allowed.'
)

addr_validator = RegexValidator(
    regex=r'^[1-9](\d|-)*( [A-Z]\w*(\w|.)?)+$',
    message='Please enter a valid address.  (Do not include the city/state/zip)'
)

id_code_validator = RegexValidator(
    regex=r'^[A-Z][A-Z]$',
    message='Id Code must be two capital letters'
)
