from django.db import models

from model.model_media import ModelMediaStoring
from model.model_util import ModelBase
from model.model_validators import id_code_validator


class Section(ModelBase, ModelMediaStoring):
    '''
        Database model for section information.  A section can be a
        building, street, bridge, shed, etc...

        Index refers to an id number for the related building
        A building index will always start at 0, if a building is added that
        conflicts with another building, it will increment the indexes from
        that number up to the highest index.  If a building is deleted, the
        higher indices will be reduced to make up for the loss

        Address referes to the street address of the building.  This DOES NOT
        include the city, state, or zip.  It is assumed that this other
        data is the same as the project
    '''

    index = models.PositiveIntegerField()
    name = models.CharField(max_length=50)
    name_simple = models.CharField(max_length=80, blank=True)
    description = models.CharField(max_length=400, blank=True)
    address = models.CharField(max_length=50, blank=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    project = models.ForeignKey(
        'Project',
        on_delete=models.CASCADE,
        related_name='sections',
        related_query_name='section'
    )
    classification = models.ForeignKey(
        'SectionClassification',
        on_delete=models.SET_NULL,
        related_name='sections',
        related_query_name='section',
        null=True
    )

    class Meta:
        ordering = ['project', 'index']

    def __str__(self):
        return '{} section {}:{}'.format(self.project, self.index, self.name)

    def save(self, force_insert=False, force_update=False):
        # Make sure that the index is correct
        try:
            # Recursively increment indices
            last = Section.objects.get(project=self.project, index=self.index)
            last.index = last.index + 1
            last.save()
        except Section.DoesNotExist:
            # If the assigned index does not exist, set it to the last index
            count = Section.objects.filter(project=self.project).count()
            self.index = count

        super(Section, self).save(force_insert, force_update)


class SectionClassification(models.Model):
    '''
        Model for different types of classification of sections

        This can be something like building, bridge, street, parking lot, etc
    '''

    id_code = models.CharField(
        max_length=2,
        unique=True,
        validators=[id_code_validator]
    )
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['id_code']
        verbose_name = 'Section Classification'
        verbose_name_plural = 'Section Classifications'

    def __str__(self):
        return "{}:{}".format(self.id_code, self.name)


class Floor(ModelBase, ModelMediaStoring):
    '''
        Database model for floor information

        index refers to an id number for the related floor
        A floor index will always start at 0, if a floor is added that
        conflicts with another floor, it will increment the indexes from
        that number up to the highest index.  If a floor is deleted, the
        higher indices will be reduced to make up for the loss
    '''

    index = models.PositiveIntegerField()
    name = models.CharField(max_length=30)
    name_simple = models.CharField(max_length=60, blank=True)
    description = models.CharField(max_length=400, blank=True)
    section = models.ForeignKey(
        'Section',
        on_delete=models.CASCADE,
        related_name='floors',
        related_query_name='floor',
        default=0
    )

    class Meta:
        ordering = ['section', 'index']

    def __str__(self):
        return '{} floor {}:{}'.format(self.building, self.index, self.name)

    def save(self, force_insert=False, force_update=False):
        # make sure that the index is correct
        try:
            # Recursively increment indices
            last = Floor.objects.get(building=self.building, index=self.index)
            last.index = last.index + 1
            last.save()
        except Floor.DoesNotExist:
            # If the assigned index does not exist, set it to the last index
            count = Floor.objects.filter(building=self.building).count()
            self.index = count

        super(Floor, self).save(force_insert, force_update)


class Room(ModelBase, ModelMediaStoring):
    '''
        Database model for room information

        index refers to an id number for the related room
        A room index will always start at 0, if a room is added that
        conflicts with another room, it will increment the indexes from
        that number up to the highest index.  If a room is deleted, the
        higher indices will be reduced to make up for the loss
    '''

    index = models.PositiveIntegerField()
    name = models.CharField(max_length=30)
    name_simple = models.CharField(max_length=60, blank=True)
    description = models.CharField(max_length=400, blank=True)
    floor = models.ForeignKey(
        'Floor',
        on_delete=models.CASCADE,
        related_name='rooms',
        related_query_name='room',
        default=0
    )

    class Meta:
        ordering = ['floor', 'index']

    def __str__(self):
        return '{} room {}:{}'.format(self.floor, self.index, self.name)

    def save(self, force_insert=False, force_update=False):
        # make sure that the index is correct
        try:
            # Recursively increment indices
            last = Room.objects.get(floor=self.floor, index=self.index)
            last.index = last.index + 1
            last.save()
        except Room.DoesNotExist:
            # If the assigned index does not exist, set it to the last index
            count = Room.objects.filter(floor=self.floor).count()
            self.index = count

        super(Room, self).save(force_insert, force_update)


class RoomClassification(models.Model):
    '''
        Model for different types of classification of room

        This can be something like mechaical, classroom, office, etc
    '''

    id_code = models.CharField(
        max_length=2,
        unique=True,
        validators=[id_code_validator]
    )
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['id_code']
        verbose_name = 'Room Classification'
        verbose_name_plural = 'Room Classifications'

    def __str__(self):
        return "{}:{}".format(self.id_code, self.name)

