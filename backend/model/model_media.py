import os, shutil
from distutils.dir_util import mkpath

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from model.model_util import ModelBase


def upload_to_loc(instance, filename):
    '''
        Defines a temporary location to upload the file to
    '''
    return 'tmp/{}'.format(filename)


class ModelMediaStoring(models.Model):
    '''
        Base model for any type of data that will be
        storing picture/videos/docuements
    '''

    images = models.ManyToManyField('Picture')
    videos = models.ManyToManyField('Video')
    docs = models.ManyToManyField('Document')

    class Meta:
        abstract=True


class Document(ModelBase):
    '''
        Model for any documentation that is not an image or movie
        IE: PDF, Excel spreadsheet, word doc
    '''

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    name_simple = models.CharField(max_length=60, blank=True)

    file_loc = models.FileField(upload_to=upload_to_loc)

    class Meta:
        ordering = ['id']
        verbose_name = 'Document'

    def __str__(self):
        return self.name

    @property
    def upload_date(self):
        return self.create_date


class ImageBase(ModelBase):
    '''
        Base class for any Image Type data

        id is the unique, autoincrementing id of the image
            images are stored in one location with the name of
            the image changed to the id with the same extension
        name is the name that the image was uploaded with
            This name will replace the name of the image when it
            is downloaded
        name_simple is an optional field for a simple, human readable name
        take_date is when the image was taken.  Extracted from image metadata
    '''

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    name_simple = models.CharField(max_length=60, blank=True)

    file_loc = models.ImageField(upload_to=upload_to_loc)

    class Meta:
        abstract=True
        ordering = ['id']

    def __str__(self):
        return '{}:{}'.format(self.id, self.name)

    @property
    def upload_date(self):
        return self.create_date


class Logo(ImageBase):
    '''
        Special image classification for logos. Proxy to the baseimage class
    '''
    class Meta:
        verbose_name = 'Logo'


class Picture(ImageBase):
    '''
        Special image classification for general picture.
        Inherits from ImageBase
    '''

    GENERAL='GE'
    PANORAMA='PA'
    FLOORPLAN='FP'
    SITEPLAN='SP'
    PICTURE_TYPE_CHOICES = (
            (GENERAL, 'General'),
            (PANORAMA, 'Panorama'),
            (FLOORPLAN, 'Floor Plan'),
            (SITEPLAN, 'Site Plan')
    )

    take_date = models.DateTimeField()
    picture_type = models.CharField(
            max_length=2,
            choices=PICTURE_TYPE_CHOICES,
            default=GENERAL
    )

    class Meta:
        verbose_name = 'Picture'


class Video(ModelBase):
    '''
        Model for any documentation that is a movie
    '''

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    name_simple = models.CharField(max_length=60, blank=True)

    file_loc = models.FileField(upload_to=upload_to_loc)

    class Meta:
        ordering = ['id']
        verbose_name = 'Video'

    def __str__(self):
        return self.name

    @property
    def upload_date(self):
        return self.create_date


def get_ext(filename):
    # Mini helper to get the extenstion of a file
    return filename.split('.')[-1]


@receiver(post_save, sender=Document)
@receiver(post_save, sender=Logo)
@receiver(post_save, sender=Picture)
@receiver(post_save, sender=Video)
def update_file(sender, instance, **kwargs):
    '''
        Since the id of a model isn't defined until save is called
        we need to implement a signal that moves and renames the file
        from the tmp upload directory to change the name to the doc id
    '''

    # Base location for adding files
    location = 'other/{}.{}'.format(instance.id, get_ext(str(instance.file_loc)))

    # Makes sure that when save is called, the file is not already correctly named
    if 'tmp/' in str(instance.file_loc):

        # Gets what the actual location of the file should be
        if instance._meta.verbose_name == 'Logo':
            location = 'logo/{}.{}'.format(instance.id, get_ext(str(instance.file_loc)))
        if instance._meta.verbose_name == 'Picture':
            location = 'images/{}.{}'.format(instance.id, get_ext(str(instance.file_loc)))
        if instance._meta.verbose_name == 'Document':
            location = 'docs/{}.{}'.format(instance.id, get_ext(str(instance.file_loc)))
        if instance._meta.verbose_name == 'Video':
            location = 'videos/{}.{}'.format(instance.id, get_ext(str(instance.file_loc)))

        # Actually moves the file
        basedir = '{}{}/'.format(settings.MEDIA_ROOT, os.path.dirname(location))
        mkpath(basedir)
        shutil.move('{}{}'.format(settings.MEDIA_ROOT, str(instance.file_loc)),
                    '{}{}'.format(settings.MEDIA_ROOT, location))

        # Updates the model with the new location
        instance.file_loc = location
        instance.save()

