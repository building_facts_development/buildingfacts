from django.db import models

from django import forms

from model.model_section import Section, Floor, Room
from model.model_util import ModelBase


class RegionBase(ModelBase):
    '''
        Base model for a place of interest in a picture
    '''

    IMAGES = 'I'
    BUILDING = 'B'
    FLOOR = 'F'
    LINK_TYPE_CHOICES = (
        (IMAGES, 'Images'),
        (BUILDING, 'Building'),
        (FLOOR, 'Floor')
    )

    x1 = models.PositiveIntegerField()
    y1 = models.PositiveIntegerField()
    x2 = models.PositiveIntegerField(null=True)
    y2 = models.PositiveIntegerField(null=True)
    base_picture = models.ForeignKey(
        'Picture',
        on_delete=models.CASCADE,
        related_name='%(class)s'
    )
    link_type = models.CharField(
        max_length=1,
        choices=LINK_TYPE_CHOICES,
        default=IMAGES
    )
    # null if the link is to images, just use the images var
    link_section = models.ForeignKey(
        'Section',
        on_delete=models.CASCADE,
        related_name='picture_regions',
        null=True
    )
    link_floor = models.ForeignKey(
        'Floor',
        on_delete=models.CASCADE,
        related_name='picture_regions',
        null=True
    )
    link_room = models.ForeignKey(
        'Room',
        on_delete=models.CASCADE,
        related_name='picture_regions',
        null=True
    )

    class Meta:
        ordering = ['base_picture', 'x1', 'y1', 'x2', 'y2']

    def __str__(self):
        return '{} at {},{}:{},{}'.format(self.base_picture, self.x1, self.y1, self.x2, self.y2)

    def clean(self):
        cleaned_data = super(RegionBase, self).clean()
        x2 = cleaned_data.get('x2')
        y2 = cleaned_data.get('y2')

        if (x2 is None and y2 is not None) or (x2 is not None and y2 is None):
            raise forms.ValidationError(
                    'x2 and y2 must either both be filled or both be empty'
            )

    def get_point_1(self):
        return (x1,y1)

    def get_point_2(self):
        return (x2,y2)


class RegionOfInterestManager(models.Manager):
    def get_queryset(self):
        return super(RegionOfInterestManager, self).get_queryset().filter(~Q(x2=None), ~Q(y2=None))


class PointOfInterestManager(models.Manager):
    def get_queryset(self):
        return super(PointOfInterestManager, self).get_queryset().filter(x2=None, y2=None)


class RegionOfInterest(RegionBase):
    '''
        Proxy class that will only return RegionBase entries that are regions
        IE: Have 2 x/y points
    '''
    objects = RegionOfInterestManager()

    class Meta:
        proxy=True
        verbose_name='Region of Interest'
        verbose_name_plural='Regions of Interest'

    def __str__(self):
        return '{} in {},{} to {},{}'.format(self.base_picture, self.x1, self.y1, self.x2, self.y2)


class PointOfInterest(RegionBase):
    '''
        Proxy class that will only return RegionBase entries that are points
        IE: Have 1 x/y point
    '''
    objects = PointOfInterestManager()

    class Meta:
        proxy=True
        verbose_name='Point of Interest'
        verbose_name_plural='Points of Interest'

    def __str__(self):
        return '{} at {},{}'.format(self.base_picture, self.x1, self.y1)

