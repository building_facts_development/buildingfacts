from django.db import models

from model.model_media import Logo, Picture
from model.model_util import ModelAddressable, ModelBase
from model.model_validators import phone_validator, id_code_validator


class Company(ModelBase, ModelAddressable):
    '''
        Database model for extra user profile information 
        beyond simple login information

        A company can have a logo and associated picture (maybe of
            the employees, or the main building, or anything that the
            company wants to do to identify itself
        And a set of classifications (General Contractor/Electrical/Plumbing)
            A company can even have mutliple classifications if it does
            multiple things
    '''

    name = models.CharField(max_length=80, unique=True)
    phone = models.CharField(
        validators=[phone_validator],
        max_length=17,
        unique=True
    )
    fax = models.CharField(
        validators=[phone_validator],
        max_length=17,
        blank=True,
        unique=True
    )
    email = models.EmailField(unique=True)
    website = models.URLField(blank=True, unique=True)
    logo = models.OneToOneField(
        'Logo',
        on_delete=models.SET_NULL,
        related_name='company',
        related_query_name='companies',
        blank=True,
        null=True
    )
    picture = models.ForeignKey(
        'Picture',
        on_delete=models.SET_NULL,
        related_name='company_picture',
        blank=True,
        null=True
    )
    classification = models.ManyToManyField(
        'CompanyClassification',
        related_name='companies'
    )

    class Meta:
        ordering = ['name']
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return self.name


class CompanyClassification(models.Model):
    '''
        Model for different types of classification of companies

        It is assumed that (GC, 'General Contractor') will be an entry
        in this database
    '''

    id_code = models.CharField(
        max_length=2,
        unique=True,
        validators=[id_code_validator]
    )
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['id_code']
        verbose_name = 'Company Classification'
        verbose_name_plural = 'Company Classifications'

    def __str__(self):
        return "{}:{}".format(self.id_code, self.name)

