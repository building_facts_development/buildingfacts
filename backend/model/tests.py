from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase

from model.model_company import Company, CompanyClassification
from model.model_project import Project, ProjectClassification
from model.model_section import Section, \
        SectionClassification,\
        Floor,\
        Room,\
        RoomClassification
from users.models import UserProfile

'''
    ALL MODEL TESTS ARE DESIGNED AROUND NON-ARBITRARY PROPERTIES

    If a field length does not have to be a specific length, it
    will not be tested for

    If fields become less arbitrary, new tests will need to be
    added

    The first section contains setup for specific parts of the
    database to prevent code duplication.  For example, both a
    Company and a UserProfile need a Company setup initially
    because a user belongs to a company.
'''


def setup_Company():
    cc = CompanyClassification(
            id_code = 'BC',
            name = 'BaseClassification'
    )

    cc.full_clean()
    cc.save()

    c = Company(
            name = 'BaseCompany',
            email = 'basecompany@email.com',
            address = '50 Base Company Dr',
            city = 'Basey',
            postal_code = '12345',
            phone = '1234567890'
    )
    c.full_clean()
    c.save()

def setup_User():
    usr = User.objects.create_user('testuser', 'user@company.com', 'password')

    profile = UserProfile(
            user = usr,
            first_name = 'first',
            last_name = 'last',
            company = Company.objects.get(pk=1)
    )
    profile.full_clean()
    profile.save()

def setup_Project():
    pc = ProjectClassification(
            id_code = 'AP',
            name = 'Appartment'
    )

    pc.full_clean()
    pc.save()

    p = Project(
            name = 'BaseProject',
            address = '50 Base Project Dr',
            city = 'Basey',
            postal_code = '12345'
    )
    p.full_clean()
    p.save()


def setup_Section():
    sc = SectionClassification(
            id_code = 'BU',
            name = 'Building'
    )

    sc.full_clean()
    sc.save()

    s = Section(
            index = 0,
            name = 'Building 1',
            project = Project.objects.get(pk=1)
    )
    s.classification = sc

    s.full_clean()
    s.save()


def setup_Floor():
    f = Floor(
            index = 0,
            name = 'Floor 1',
            section = Project.objects.get(pk=1)
    )

    f.full_clean()
    f.save()


def setup_Room():
    rc = SectionClassification(
            id_code = 'CR',
            name = 'Classroom'
    )

    rc.full_clean()
    rc.save()

    r = Room(
            index = 0,
            name = 'Test Room',
            floor = Floor.objects.get(pk=1)
    )

    r.full_clean()
    r.save()


class CompanyTest(TestCase):
    '''
        Tests that operate on the company model
    '''

    def setUp(self):
        setup_Company()
        setup_User()

    def test_unique_fields(self):
        try:
            c = Company.objects.get(pk=1)
            c.pk = None

            c.full_clean()
            c.save()
        except ValidationError as e:
            self.assertEqual(len(e.message_dict), 5)
            self.assertTrue('name' in e.message_dict)
            self.assertTrue('phone' in e.message_dict)
            self.assertTrue('fax' in e.message_dict)
            self.assertTrue('website' in e.message_dict)
            self.assertTrue('email' in e.message_dict)

    def test_sorting(self):
        c = Company.objects.get(pk=1)
        c.pk = None
        c.name = 'Z Company'
        c.phone = '1111111111'
        c.fax = '1111111111'
        c.email = 'email1@email.com'
        c.website = 'https://www.google.com'
        c.full_clean()
        c.save()

        c.pk = None
        c.name = 'Company 2'
        c.phone = '2222222222'
        c.fax = '2222222222'
        c.email = 'email2@email.com'
        c.website = 'https://www.company.com'
        c.full_clean()
        c.save()

        c.pk = None
        c.name = 'Company 1'
        c.phone = '3333333333'
        c.fax = '3333333333'
        c.email = 'email3@email.com'
        c.website = 'https://www.completelyawebsite.com'
        c.full_clean()
        c.save()

        self.assertEqual(Company.objects.count(), 4)

        lst = Company.objects.all()

        self.assertEqual(lst[0].name, 'BaseCompany')
        self.assertEqual(lst[1].name, 'Company 1')
        self.assertEqual(lst[2].name, 'Company 2')
        self.assertEqual(lst[3].name, 'Z Company')

    def test_logo_del(self):
        pass

    def test_picture_del(self):
        pass

    def test_default_class(self):
        return
        c = Company.objects.get(pk=1)
        default = CompanyClassification.objects.get(pk=1)
        self.assertTrue(default in c.classification.all())

    def test_class_unique(self):
        try:
            c = CompanyClassification.objects.get(pk=1)
            c.pk = None

            c.full_clean()
            c.save()
        except ValidationError as e:
            self.assertTrue('id_code' in e.message_dict)
            self.assertTrue('name' in e.message_dict)


class UserProfileTest(TestCase):
    '''
        Tests that operate on the UserProfile model
    '''

    def setUp(self):
        setup_Company()
        setup_User()

    def test_full_name(self):
        '''
            The full name property returns correctly
        '''
        fn = UserProfile.objects.get(pk=1).full_name
        self.assertEqual(fn, 'first last')

    def test_onetoone_user(self):
        '''
            The User should have a one to one relationship with UserProfile
        '''
        try:
            u = UserProfile.objects.get(pk=1)
            u.pk = None
            u.full_clean()
            u.save()
        except ValidationError as e:
            self.assertEqual(len(e.message_dict), 1)
            self.assertTrue('user' in e.message_dict)
        

    def test_sorting(self):
        '''
            Sorting by lastname, then first name, then pk
        '''
        u = UserProfile.objects.get(pk=1)

        usr = User.objects.create_user('testuser1', 'user1@company.com', 'password')
        u.user = usr
        u.pk = None
        u.last_name = 'last'
        u.first_name = 'z'
        u.full_clean()
        u.save()

        usr = User.objects.create_user('testuser2', 'user2@company.com', 'password')
        u.user = usr
        u.pk = None
        u.last_name = 'last'
        u.first_name = 'first'
        u.full_clean()
        u.save()

        usr = User.objects.create_user('testuser3', 'user3@company.com', 'password')
        u.user = usr
        u.pk = None
        u.last_name = 'first'
        u.first_name = 'last'
        u.full_clean()
        u.save()

        self.assertEqual(UserProfile.objects.count(), 4)

        lst = UserProfile.objects.all()

        self.assertEqual(lst[0].pk, 4)
        self.assertEqual(lst[1].pk, 1)
        self.assertEqual(lst[2].pk, 3)
        self.assertEqual(lst[3].pk, 2)


class ProjectTest(TestCase):
    '''
        Tests that operate on the Project model
    '''

    def setUp(self):
        setup_Company()
        setup_User()

    def test_default_classification(self):
        pass

    def test_required_classification(self):
        pass

    def test_end_without_start(self):
        pass

    def test_end_before_start(self):
        pass

    def test_del_logo_pro(self):
        pass

    def test_del_logo_prop(self):
        pass

    def test_del_pic_prop(self):
        pass

    def test_one_company_req(self):
        pass

    def test_sorting(self):
        pass

    def test_classification_order(self):
        pass

    def test_classification_sort(self):
        pass


class SectionTest(TestCase):
    '''
        Tests that operate on the Project model
    '''

    def setUp(self):
        setup_Company()
        setup_User()
        setup_Project()
        setup_Section()

    def test_default_classification(self):
        pass

    def test_delete_prop(self):
        pass

    def test_end_without_start_date(self):
        pass

    def test_end_after_start_date(self):
        pass

    def test_start_before_project_start(self):
        pass

    def test_end_after_project_end(self):
        pass

    def test_sorting(self):
        pass

    def test_indexing(self):
        pass

    def test_classification_unique(self):
        pass

    def test_classification_sorting(self):
        pass


class FloorTest(TestCase):
    '''
        Tests that operate on the Project model
    '''

    def setUp(self):
        setup_Company()
        setup_User()
        setup_Project()
        setup_Section()

    def test_default_classification(self):
        pass

    def test_delete_prop(self):
        pass

    def test_sorting(self):
        pass

    def test_indexing(self):
        pass


class RoomTest(TestCase):
    '''
        Tests that operate on the Project model
    '''

    def setUp(self):
        setup_Company()
        setup_User()
        setup_Project()
        setup_Section()

    def test_default_classification(self):
        pass

    def test_delete_prop(self):
        pass

    def test_sorting(self):
        pass

    def test_indexing(self):
        pass

    def test_classification_unique(self):
        pass

    def test_classification_sorting(self):
        pass


class DocumentTest(TestCase):
    pass


class LogoTest(TestCase):
    pass


class PictureTest(TestCase):
    pass


class VideoTest(TestCase):
    pass


class RegionOfInterestTest(TestCase):
    pass


class PointOfInterestTest(TestCase):
    pass


class ValidatorTest(TestCase):
    pass

