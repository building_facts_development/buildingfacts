from django.db import models

from model.model_company import Company
from model.model_media import ModelMediaStoring, Picture
from model.model_util import ModelAddressable, ModelBase
from model.model_validators import id_code_validator


class ProjectClassification(models.Model):
    '''
        Model for different types of classification of projects

        Can be used to implement analytics for types of projects 
        worked on
    '''
    DEFAULT_PROJECT_CLASSIFICATION = 1

    id_code = models.CharField(
        max_length=2,
        unique=True,
        validators=[id_code_validator]
    )
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['id_code']
        verbose_name = 'Project Classification'
        verbose_name_plural = 'Project Classifications'

    def __str__(self):
        return '{} {}'.format(self.id_code, self.name)


class Project(ModelBase, ModelAddressable, ModelMediaStoring):
    '''
        Database model for a whole project description
    '''

    name = models.CharField(max_length=50)
    name_simple = models.CharField(max_length=80, blank=True)
    description = models.CharField(max_length=400, blank=True)
    contract_number = models.CharField(max_length=30, blank=True)
    logo = models.ForeignKey(
        'Logo',
        on_delete=models.SET_NULL,
        related_name='project_logos',
        related_query_name='project_logo',
        blank=True,
        null=True
    )
    picture = models.ForeignKey(
        'Picture',
        on_delete=models.SET_NULL,
        related_name='project_base_pictures',
        related_query_name='project_base_picture',
        blank=True,
        null=True
    )
    classification = models.ForeignKey(
        'ProjectClassification',
        on_delete=models.SET_DEFAULT,
        related_name='projects',
        related_query_name='project',
        default=ProjectClassification.DEFAULT_PROJECT_CLASSIFICATION,
        blank=True,
        null=True
    )

    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)

    project_admins = models.ManyToManyField(
        'users.UserProfile',
        related_name='admin_projects',
        related_query_name='admin_project'
    )
    project_viewers = models.ManyToManyField(
        'users.UserProfile',
        related_name='viewing_projects',
        related_query_name='viewing_project'
    )
    companies = models.ManyToManyField(
        'Company',
        related_name='projects',
        related_query_name='project'
    )

    class Meta:
        ordering = ['name', 'date_start']

    def __str__(self):
        return self.name

