from django.db import models

from model.model_validators import addr_validator


class ModelBase(models.Model):
    '''
        Base Model to inherit for field analytics
    '''
    DEFAULT_USER_ID = 1

    create_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        abstract=True


class ModelAddressable(models.Model):
    '''
        Base model for any type of data that might have an
        address associated with it
    '''

    address = models.CharField(max_length=100, validators=[addr_validator])
    city = models.CharField(max_length=40)
    state = models.CharField(max_length=2, blank=True)
    postal_code = models.CharField(max_length=20)

    class Meta:
        abstract=True

