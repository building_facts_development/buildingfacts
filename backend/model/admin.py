from django.contrib import admin
from django.contrib.auth.models import User

from model.model_section import Section, Floor, Room
from model.model_company import Company, CompanyClassification
from model.model_media import Document, Logo, Picture, Video
from model.model_project import Project, ProjectClassification
from model.model_regions import PointOfInterest, RegionOfInterest


admin.site.register(Company)
admin.site.register(CompanyClassification)
admin.site.register(Project)
admin.site.register(ProjectClassification)
admin.site.register(Section)
admin.site.register(Floor)
admin.site.register(Room)

admin.site.register(Document)
admin.site.register(Picture)
admin.site.register(Logo)
admin.site.register(Video)
admin.site.register(PointOfInterest)
admin.site.register(RegionOfInterest)

