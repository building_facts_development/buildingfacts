from django.contrib.auth.models import User
from django.test import Client, TestCase
from testfixtures import Replace, test_datetime


class AuthTest(TestCase):

    c = Client(enforce_csrf_checks=True)

    def setUp(self):
        User.objects.create_user('validuser', 'user@company.com', 'validpassword')

    def test_login_valid(self):
        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'password': 'validpassword'}
        )

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('token' in resp.json())

    def test_login_invalid_user(self):
        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'invaliduser', 'password': 'validpassword'}
        )

        self.assertEqual(resp.status_code, 404)
        self.assertTrue('error' in resp.json())
        self.assertTrue('Invalid Credentials' in resp.json().get('error'))

        resp = self.c.post(
                '/api/v1/login/',
                {'user': 'validuser', 'password': 'validpassword'}
        )

        self.assertEqual(resp.status_code, 400)
        self.assertTrue('error' in resp.json())
        self.assertTrue('Please provide' in resp.json().get('error'))

    def test_login_invalid_pass(self):
        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'password': 'invalidpassword'}
        )

        self.assertEqual(resp.status_code, 404)
        self.assertTrue('error' in resp.json())
        self.assertTrue('Invalid Credentials' in resp.json().get('error'))

        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'pass': 'validpassword'}
        )

        self.assertEqual(resp.status_code, 400)
        self.assertTrue('error' in resp.json())
        self.assertTrue('Please provide' in resp.json().get('error'))

    def test_login_after_login(self):
        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'password': 'validpassword'}
        )

        token1 = resp.json().get('token')

        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'password': 'validpassword'}
        )

        token2 = resp.json().get('token')

        self.assertTrue(token1 is not token2)

    def test_logout_with_key(self):
        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'password': 'validpassword'}
        )

        token = resp.json().get('token')

        resp = self.c.post(
                '/api/v1/logout/',
                HTTP_AUTHORIZATION='Token '+token
        )

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('success' in resp.json())

    def test_logout_without_key(self):
        resp = self.c.post(
                '/api/v1/logout/'
        )

        self.assertEqual(resp.status_code, 401)

    def test_logout_invalid_key(self):
        resp = self.c.post(
                '/api/v1/login/',
                {'username': 'validuser', 'password': 'validpassword'}
        )

        token = resp.json().get('token')

        resp = self.c.post(
                '/api/v1/logout/',
                HTTP_AUTHORIZATION='Token '+token+'fail'
        )

        self.assertEqual(resp.status_code, 401)

    def test_time_invalidate(self):
        with Replace('login.authentication.datetime',
                     test_datetime(None)) as d:
            d.add(2019,4,20,1,23,45)
            d.add(2019,4,20,13,23,46)

            resp = self.c.post(
                    '/api/v1/login/',
                    {'username': 'validuser', 'password': 'validpassword'}
            )

            token = resp.json().get('token')

            resp = self.c.post(
                    '/api/v1/logout/',
                    HTTP_AUTHORIZATION='Token '+token
            )

            self.assertEqual(resp.status_code, 200)

