from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
        HTTP_400_BAD_REQUEST,
        HTTP_404_NOT_FOUND,
        HTTP_200_OK
)
from rest_framework.response import Response


@csrf_exempt
@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):
    '''
        Endpoint for allowing a user to log in and get an API
        key for access to the rest of the backend data

        Any time login is called, it will refresh the API key
    '''

    username = request.data.get('username')
    password = request.data.get('password')

    if username is None or password is None:
        return Response({'error': 'Please provide both a username and password'},
                        status=HTTP_400_BAD_REQUEST)

    user = authenticate(username=username, password=password)

    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)

    try:
        token = Token.objects.get(user=user)
        token.delete()
    except Token.DoesNotExist:
        pass

    token = Token.objects.create(user=user)

    return Response({'token': token.key},
                    status=HTTP_200_OK)


@csrf_exempt
@api_view(['POST'])
def logout(request):
    '''
        Endpoint for allowing a user to log off and clear their
        current API key

        The user must be logged in if they wish to log out
    '''

    user = request.user

    token = Token.objects.get(user=user)
    token.delete()

    return Response({'success': 'User is now logged out'},
                    status=HTTP_200_OK)

