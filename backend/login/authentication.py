from datetime import datetime, timedelta

from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions


class TimeoutAuthentication(TokenAuthentication):
    expiration = 12

    def authenticate(self, request):
        # Runs normal authenticatino
        user_token = super(TimeoutAuthentication, self).authenticate(request)

        if user_token is None:
            return None

        user, token = user_token

        # Checks if the time is beyond the expiration
        now = datetime.utcnow().replace(tzinfo=None)
        delta = timedelta(hours=self.expiration)
        created = token.created.replace(tzinfo=None)

        if created + delta < now:
            token.delete()
            
            raise exceptions.AuthenticationFailed('Authentication key expired')

        return user, token

